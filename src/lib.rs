extern crate serde;

use serde::{Serialize, Deserialize};
use serde_json::Result;
use reqwest::blocking::Client as HTTPClient;
use chrono::prelude::*;

macro_rules! get_req {
    ( $client:expr, $path:expr, $params:expr ) => {{
        let client: Client = $client;
        let path: String = format!("{}/api/v1{}", client.url, $path);
        let params: Vec<(&str, &str)> = $params;

        Ok(
            serde_json::from_str(
                HTTPClient::new()
                    .get(path)
                    .bearer_auth(client.token)
                    .query(&params)
                    .send().unwrap()
                    .text().unwrap().as_str()
            ).unwrap()
        )
    }}
}

pub fn courses(c: Client) -> Result<Vec<Course>> {
    get_req!{c, "/courses", vec![
        ("state[]", "available"),
        ("enrollment_state", "active"),
        ("include[]", "total_students"),
        ("include[]", "syllabus_body"),
        ("include[]", "course_image"),
        //("include[]", "total_scores")
    ]}
}

#[derive(Clone)]
pub struct Client {
    pub url: &'static str,
    pub token: String
}
impl Default for Client {
    fn default() -> Self {
        Self {
            url: "",
            token: "".to_string()
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Course {
    pub id:             i64,
    pub name:           Option<String>,
    pub course_code:    Option<String>,
    pub original_name:  Option<String>,
    pub start_at:       Option<DateTime<Utc>>,
    pub end_at:         Option<DateTime<Utc>>,
    pub total_students: Option<i64>,
    pub calendar:       Option<Calendar>,
    #[serde(rename = "image_download_url")]
    pub image_url:      Option<String>,
    pub default_view:   Option<DefaultView>,
    pub syllabus_body:  Option<String>
}

#[derive(Serialize, Deserialize, Clone)]
/// A [User] is a real person (or test student)
pub struct User {
    pub id:         i64,
    pub name:       String,
    pub short_name: String,
    pub avatar_url: String,
    /// Only appears if the [User] has their pronouns set
    // Why are there only 3 options?
    pub pronouns:   Option<String>,
    pub email:      Option<String>,
    /// Only appears if the [User] has their bio set
    // Why do I not have the option to set this?
    pub bio:        Option<String>
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Assignment {
    pub id:          i64,
    pub name:        String,
    pub description: Option<String>,
    pub due_at:      Option<DateTime<Utc>>
}

#[derive(Serialize, Deserialize, Clone)]
/// An `.ics` calendar
pub struct Calendar {
    /// URL to the `.ics` file
    pub ics: String
}

#[derive(Serialize, Deserialize, Clone)]
/// Teacher-selected `default_view` for a [Course]
pub enum DefaultView {
    #[serde(rename = "feed")]
    /// `feed` page
    Feed,
    #[serde(rename = "wiki")]
    /// `wiki` page
    Wiki,
    #[serde(rename = "modules")]
    /// `modules` page
    Modules,
    #[serde(rename = "assignments")]
    /// `assignments` page
    Assignments,
    #[serde(rename = "syllabus")]
    /// `syllabus` page
    Frontpage
}

impl Assignment {
    /// Returns a string indicating the number of days/weeks until an [Assignment] is due.
    /// 
    /// If the [Assignment] is overdue, this instead returns the number of days since the [Assignment] was due.
    pub fn due_in(&self) -> String {
        let due = self.due_at.unwrap();
        let now: DateTime<Utc> = Utc::now();

        let days: i64 = <i64 as From<u32>>::from(due.day()) - <i64 as From<u32>>::from(now.day());
        let weeks = due.iso_week().week() - now.iso_week().week();

        if days <= -1 {
            if days < -1 {
                format!("{} days late", days.abs())
            } else {
                "a day late".to_string()
            }
        } else if days < 7 {
            if days > 1 {
                format!("due in {} days", days)
            } else {
                "due tomorrow".to_string()
            }
        } else {
            if days > 7 {
                format!("{} weeks", weeks)
            } else {
                "a week".to_string()
            }
        }
    }
}

const ASSIGMENTS_LIMIT: &'static str = "50";

impl Course {
    pub fn peers(&self, c: Client) -> Result<Vec<User>> {
        let total_students = format!("{}", self.total_students.unwrap());
        get_req!{
            c,
            format!("/courses/{}/users", self.id),
            vec![
                ("include[]", "avatar_url"),
                ("include[]", "bio"),
                ("include[]", "custom_links"),
                ("enrollment_type[]", "student"),
                ("limit", total_students.as_str())
            ]
        }
    }
    /// Gets past [Assignment]s
    pub fn past_assignments(&self, c: Client) -> Result<Vec<Assignment>> {
        get_req!{
            c,
            format!("/courses/{}/assignments", self.id),
            vec![
                ("limit", ASSIGMENTS_LIMIT),
                ("bucket", "past")
            ]
        }
    }
    /// Gets overdue [Assignment]s
    pub fn overdue_assignments(&self, c: Client) -> Result<Vec<Assignment>> {
        get_req!{
            c,
            format!("/courses/{}/assignments", self.id),
            vec![
                ("limit", ASSIGMENTS_LIMIT),
                ("bucket", "overdue")
            ]
        }
    }
    /// Gets upcoming [Assignment]s
    pub fn upcoming_assignments(&self, c: Client) -> Result<Vec<Assignment>> {
        get_req!{
            c,
            format!("/courses/{}/assignments", self.id),
            vec![
                ("limit", ASSIGMENTS_LIMIT),
                ("bucket", "upcoming")
            ]
        }
    }
    /// Gets future [Assignment]s
    pub fn future_assignments(&self, c: Client) -> Result<Vec<Assignment>> {
        get_req!{
            c,
            format!("/courses/{}/assignments", self.id),
            vec![
                ("limit", ASSIGMENTS_LIMIT),
                ("bucket", "future")
            ]
        }
    }
}
