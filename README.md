
# canvaslms

An easy-to-use client for the Canvas LMS REST API written in Rust.

```rust
let client = Client {
    url: "https://yourschool.instructure.com",
    token: "12345~dmeeDJWJNWjdjwkdjk77483dmKD"
};

for c in courses(client).unwrap() {
    println!("{}", c.clone().name.unwrap());

    for a in c.overdue_assignments(client).unwrap() {
        if a.due_at.is_some() {
            println!(" ! {} ({})", a.name, a.due_in());
        }
    }
    for a in c.upcoming_assignments(client).unwrap() {
        if a.due_at.is_some() {
            println!(" - {} ({})", a.name, a.due_in());
        }
    }
}
```
